#!/bin/bash

result_file=m_screenshots.yaml
base_url=https://f-droid.org/repo/
work_dir=/tmp/tmprepo/
rm -f /tmp/$result_file
touch /tmp/$result_file
MYCUSTOMTAB='  '

env/get_package_list.py metadata/index-v1.json | sort -u > /tmp/packages.txt
for p in `cat /tmp/packages.txt`
do
	echo processing $p  ...
	srcurl=`env/get_package_list.py metadata/index-v1.json --get-latest-app-tarball $p`
    url=$base_url$srcurl
	echo source URL is $url
	mkdir -p $work_dir
	pushd $work_dir
	# wget $srcurl

	# download done do all the work on it
	mkdir -p work
	cd work
	tar xf ../$srcurl
	cd $srcurl
	# tar xf ../pw.thedrhax.mosmetro_71_src.tar.gz # testfile
	# cd pw.thedrhax.mosmetro_71_src.tar.gz        # testfile

	#ss=`find metadata fastlane -iname "*jpg" ; find metadata fastlane -iname "*jpeg" ;find metadata fastlane -iname "*png"`
	# find also screenshots that are not in fastlane
	ss=`find . | grep -i screenshot | egrep "jpg$|png$|jpeg$" | sed 's/ /%20/g'`

	echo $p: >> /tmp/$result_file
	for s in $ss
	do
		# remove possible package name from string
		s=`echo $s | sed -e "s/^.\/.*_src//"`
		echo storing result in $result_file result is: $s
		echo "${MYCUSTOMTAB}- $s" >> /tmp/$result_file
	done
	echo   >> /tmp/$result_file
	cd ..
	cd ..
	rm -rf work
	popd
	# break
	continue
done

cp -a /tmp/m_screenshots.yaml metadata/

